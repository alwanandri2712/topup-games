<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LayananKeuangan extends Model
{
    use HasFactory;
    protected $guarded = [];
}
