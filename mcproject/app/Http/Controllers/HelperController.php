<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HelperController extends Controller
{
    public function province()
    {
        $getAPI = file_get_contents("https://dev.farizdotid.com/api/daerahindonesia/provinsi");
        return $getAPI;
    }

    public function kota($id)
    {
        $getAPI = file_get_contents("https://dev.farizdotid.com/api/daerahindonesia/kota?id_provinsi=$id");

        return $getAPI;
    }

    public function kecamatan($id)
    {
        $getAPI = file_get_contents("https://dev.farizdotid.com/api/daerahindonesia/kecamatan?id_kota=$id");

        return $getAPI;
    }

    public function kelurahan($id)
    {
        $getAPI = file_get_contents("https://dev.farizdotid.com/api/daerahindonesia/kelurahan?id_kecamatan=$id");

        return $getAPI;
    }
}
