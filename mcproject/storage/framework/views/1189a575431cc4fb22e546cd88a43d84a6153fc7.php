<?php $__env->startSection('custom_style'); ?>


<style>
  .accordion-button {
      box-shadow:none!important;
  }
  .accordion-tipe{
      font-weight:600;
  }
  .btn.disabled,.btn:disabled,fieldset:disabled{
      background:#8ba4b1;border-color:#8ba4b1;
  }
  .product .box{
      margin-bottom:40px;
  }
  .games-banner{
      height:170px;
      background:url(<?php echo e($kategori->banner); ?>);
      background-size:cover;
      background-repeat:no-repeat;
      background-size:100%;
  }
  .num-page{
      margin-bottom:10px;
  }
  .num-page div{
      width:40px;
      height:40px;
      border-radius:50%;
      text-align:center;
      font-size:1.875rem;
      background:var(--warna_3);
      color:#fff;
      line-height:40px;
      float:left;
  }
  .num-page p{
      margin-left:5px;
      display:inline-block;
      font-size:1.25rem;
      font-weight:500;
      padding-top:6px;
  }
  .num-page i {
      font-size: 16px;
      margin-top:13px;
      margin-left:5px;
  }

  button.accordion-button{
      outline:none!important;
      border:none!important;
      box-shadow:none!important;
  }
  .box-back i{
      font-size:22px;
      margin-top:2px;
      color:#fff;
  }
  .product-list{
      border-radius:.5rem;
      box-shadow: 0 4px 6px -1px rgba(0, 0, 0, 0.1),0 2px 4px -1px rgba(0, 0, 0, 0.06);
      overflow:hidden;
      border:1px solid var(--warna_3)!important;
  }
  .product-list b{
      font-size:.85rem;font-weight:600;
  }
  .product-list span{
      font-size:11px;color:#fff;
  }
  .product-list.active{
      background: var(--warna_3);
      border:1px solid var(--warna_3)!important;
  }
  .product-list.active:before{
      display:inline-block;
      content:'L';
      position:relative;
      background:#212121;
      margin-left:-20px;
      height:53px;
      line-height:40px;
      width:20px;
      text-align:center;
      color:#fff;
      top:-23px;
      transform:rotate(45deg) scaleX(-1);
  }
  .product-list.active b{
      margin-top:-53px;
  }
  .row {
      display: flex;
      flex-wrap: wrap;
      margin-top: calc(-1 * var(--bs-gutter-y));
      margin-right: 0;
      margin-left: 0;
  }
  .bg-product {
      background: var(--warna_3);
  }
  .panduan {
      color: var(--warna_4);
      font-size: 0.85rem;
      margin-top: 0.5rem;
  }
  .swal2-styled.swal2-confirm {
      background-color: var(--warna_2)!important;
      color: #fff;
  }
  .swal2-styled.swal2-confirm:focus {
      box-shadow: none!important;
  }
  .product-list img {
      display: flex;
      float: right;
      margin-top: -12px;
  }
  .productlogo {
      width: 32px;
      right: 5%;
  }
  .accordion-button:hover {
      z-index: 0;
  }
  .bg-payment {
      background: #E6E7EB;
  }
  .method-list.active {
      border-color: #3b3b3b!important;
  }
  .method-list.active:before {
      background: #3b3b3b!important;
  }

  .btn-order {
      display: inline-block;
      border: 0;
      outline: 0;
      padding: 12px 16px;
      line-height: 1.4;
      cursor: pointer;
      /* Important part */
      position: relative;
      transition: padding-right .3s ease-out;
              
          }
          
  .btn-order.loading {
      padding-right: 40px;
  }

  .btn-order.loading:after {
      content: "";
      position: absolute;
      border-radius: 100%;
      right: 10px;
      top: 35%;
      width: 0px;
      height: 0px;
      border: 2px solid rgba(255,255,255,0.5);
      border-left-color: #FFF;
      border-top-color: #FFF;
      animation: spin .6s infinite linear, grow .3s forwards ease-out;
  }
  @keyframes    spin { 
      to {
          transform: rotate(359deg);
      }
  }
  @keyframes    grow { 
      to {
          width: 16px;
          height: 16px;
      }
  }

  .shadow-form {
      box-shadow: 0 4px 80px hsla(0,0%,77%,.13), 0 1.6711px 33.4221px hsla(0,0%,77%,.093), 0 0.893452px 17.869px hsla(0,0%,77%,.077), 0 0.500862px 10.0172px hsla(0,0%,77%,.065), 0 0.266004px 5.32008px hsla(0,0%,77%,.053), 0 0.11069px 2.21381px hsla(0,0%,77%,.037);
  }
</style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

<?php if(Auth::check()): ?>
    <?php if(Auth()->user()->role == 'Member' || Auth()->user()->role == 'Platinum' || Auth()->user()->role == 'Gold'): ?>
<nav class="navbar navbar-expand-lg d-flex fixed-top shadow">
  <div class="container-fluid">
    <a class="navbar-brand" href="#">
        <img src="<?php echo e(url('')); ?><?php echo e(!$config ? '' : $config->logo_header); ?>" alt="" width="100" onclick="window.location='<?php echo e(url('')); ?>'">
    </a>
    <div class="search-item">
                    <div class="">
                        <div class="nav-item dropdown">
                            <div class="input-group search-bar" aria-haspopup="true" id="dropsearchdown" aria-expanded="false">
                                <span class="input-group-text" id="basic-addon1"><i class="fa-solid fa-magnifying-glass"></i></span>
                                    <input type="text" name="q" placeholder="Cari..." id="searchProds" class="form-control input-box" autocomplete="off">
                            </div>
                        </div>
                    </div>
                </div>
            
            <div class="hasil-cari">
                <ul class="position-absolute resultsearch shadow dropdown-menu" aria-labelledby="dropsearchdown"></ul>
            </div>
    
            <button class="navbar-toggler border-0 d-lg-none" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasDarkNavbar" aria-controls="offcanvasDarkNavbar">
              <span><i class="fa fa-bars-staggered text-light"></i></span>
            </button>
            <div class="offcanvas offcanvas-end w-75" tabindex="-1" id="offcanvasDarkNavbar" aria-labelledby="offcanvasDarkNavbarLabel">
              <div class="offcanvas-header">
                <h5 class="offcanvas-title" id="offcanvasDarkNavbarLabel">
                    <img src="<?php echo e(url('')); ?><?php echo e(!$config ? '' : $config->logo_header); ?>" alt="" width="100" onclick="window.location='<?php echo e(url('')); ?>'">
                </h5>
                <button type="button" class="btn-close btn-close-white" data-bs-dismiss="offcanvas" aria-label="Close"></button>
              </div>
		      <div class="offcanvas-body d-lg-none">
                <ul class="navbar-nav justify-content-end flex-grow-1 pe-3">
          <li class="nav-item">
            <a class="nav-link" href="<?php echo e(url('')); ?>"><i class="fa-solid fa-house"></i> Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo e(url('/cari')); ?>"><i class="fa-solid fa-magnifying-glass"></i> Cek Pesanan</a>
          </li>
                    <li class="nav-item">
            <a class="nav-link" href="<?php echo e(url('/riwayat-pembelian')); ?>"><i class="fa-solid fa-clock-rotate-left"></i> Riwayat Pembelian</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo e(url('/deposit')); ?>"><i class="fa-solid fa-wallet"></i> Top Up Saldo</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo e(url('/user/edit/profile')); ?>"><i class="fa-solid fa-user-pen"></i> Edit Profile</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo e(url('/membership')); ?>"><i class="fa-solid fa-circle-up"></i> Upgrade Membership</a>
          </li>
          <div class="card bg-card mt-2 mb-2">
            <div class="card-body">
                <span class="py-1 px-2 float-end rounded bg-warning text-dark" style="font-size: 12px;"><?php echo e(Str::title(Auth()->user()->role)); ?></span>
                <h5 class="card-title"><?php echo e(Str::title(Auth()->user()->name)); ?></h5>
                <p class="card-text">Rp <?php echo e(number_format(Auth::user()->balance, 0, ',', '.')); ?></p>
             </div>
          </div>
                    
          <div class"mt-2">
                                                          </div>
                        <button onclick="logout();" class="btn bg-white border-0 text-danger mt-2">Logout</button>
                    </ul>
    </div>
  </div>
    <div class="collapse navbar-collapse text-right d-none d-md-none d-lg-block">
      <div class="navbar-nav ms-auto nav-stacked">
        <a class="nav-link" href="<?php echo e(url('')); ?>"><i class="fa-solid fa-house"></i> Home</a>
        <a class="nav-link" href="<?php echo e(url('/cari')); ?>"><i class="fa-solid fa-magnifying-glass""></i> Cek Pesanan</a>
        <a class="nav-link text-primary" href="<?php echo e(url('/dashboard')); ?>"><i class="fa-solid fa-arrow-right-to-bracket""></i> Dashboard</a>
</div>
  </div>
</nav>
<?php else: ?>
<nav class="navbar navbar-expand-lg d-flex fixed-top shadow">
  <div class="container-fluid">
    <a class="navbar-brand" href="#">
        <img src="<?php echo e(url('')); ?><?php echo e(!$config ? '' : $config->logo_header); ?>" alt="" width="100" onclick="window.location='<?php echo e(url('')); ?>'">
    </a>
    <div class="search-item">
                    <div class="">
                        <div class="nav-item dropdown">
                            <div class="input-group search-bar" aria-haspopup="true" id="dropsearchdown" aria-expanded="false">
                                <span class="input-group-text" id="basic-addon1"><i class="fa-solid fa-magnifying-glass"></i></span>
                                    <input type="text" name="q" placeholder="Cari..." id="searchProds" class="form-control input-box" autocomplete="off">
                            </div>
                        </div>
                    </div>
                </div>
            
            <div class="hasil-cari">
                <ul class="position-absolute resultsearch shadow dropdown-menu" aria-labelledby="dropsearchdown"></ul>
            </div>
    
            <button class="navbar-toggler border-0 d-lg-none" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasDarkNavbar" aria-controls="offcanvasDarkNavbar">
              <span><i class="fa fa-bars-staggered text-light"></i></span>
            </button>
            <div class="offcanvas offcanvas-end w-75" tabindex="-1" id="offcanvasDarkNavbar" aria-labelledby="offcanvasDarkNavbarLabel">
              <div class="offcanvas-header">
                <h5 class="offcanvas-title" id="offcanvasDarkNavbarLabel">
                    <img src="<?php echo e(url('')); ?><?php echo e(!$config ? '' : $config->logo_header); ?>" alt="" width="100" onclick="window.location='<?php echo e(url('')); ?>'">
                </h5>
                <button type="button" class="btn-close btn-close-white" data-bs-dismiss="offcanvas" aria-label="Close"></button>
              </div>
		      <div class="offcanvas-body d-lg-none">
                <ul class="navbar-nav justify-content-end flex-grow-1 pe-3">
          <li class="nav-item">
            <a class="nav-link" href="<?php echo e(url('')); ?>"><i class="fa-solid fa-house"></i> Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo e(url('/cari')); ?>"><i class="fa-solid fa-magnifying-glass"></i> Cek Pesanan</a>
          </li>
          <li class="nav-item">
            <a class="nav-link text-primary" href="<?php echo e(url('/dashboard')); ?>"><i class="fa-solid fa-arrow-right-to-bracket"></i> Dashboard</a>
          </li>
                    </ul>
    </div>
  </div>
    <div class="collapse navbar-collapse text-right d-none d-md-none d-lg-block">
      <div class="navbar-nav ms-auto nav-stacked">
        <a class="nav-link" href="<?php echo e(url('')); ?>"><i class="fa-solid fa-house"></i> Home</a>
        <a class="nav-link" href="<?php echo e(url('/cari')); ?>"><i class="fa-solid fa-magnifying-glass""></i> Cek Pesanan</a>
        <a class="nav-link text-primary" href="<?php echo e(url('/dashboard')); ?>"><i class="fa-solid fa-arrow-right-to-bracket""></i> Dashboard</a>
</div>
  </div>
</nav>
<?php endif; ?>
<?php else: ?>

<nav class="navbar navbar-expand-lg d-flex fixed-top shadow">
            <div class="container-fluid">
                <a class="navbar-brand" href="#">
        <img src="<?php echo e(url('')); ?><?php echo e(!$config ? '' : $config->logo_header); ?>" alt="" width="100" onclick="window.location='<?php echo e(url('')); ?>'">
    </a>
    <div class="search-item">
                    <div class="">
                        <div class="nav-item dropdown">
                            <div class="input-group search-bar" aria-haspopup="true" id="dropsearchdown" aria-expanded="false">
                                <span class="input-group-text" id="basic-addon1"><i class="fa-solid fa-magnifying-glass"></i></span>
                                    <input type="text" name="q" placeholder="Cari..." id="searchProds" class="form-control input-box" autocomplete="off">
                            </div>
                        </div>
                    </div>
                </div>
            
            <div class="hasil-cari">
                <ul class="position-absolute resultsearch shadow dropdown-menu" aria-labelledby="dropsearchdown"></ul>
            </div>
    
            <button class="navbar-toggler border-0 d-lg-none" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasDarkNavbar" aria-controls="offcanvasDarkNavbar">
              <span><i class="fa fa-bars-staggered text-light"></i></span>
            </button>
            <div class="offcanvas offcanvas-end w-75" tabindex="-1" id="offcanvasDarkNavbar" aria-labelledby="offcanvasDarkNavbarLabel">
              <div class="offcanvas-header">
                <h5 class="offcanvas-title" id="offcanvasDarkNavbarLabel">
                    <img src="<?php echo e(url('')); ?><?php echo e(!$config ? '' : $config->logo_header); ?>" alt="" width="100" onclick="window.location='<?php echo e(url('')); ?>'">
                </h5>
                <button type="button" class="btn-close btn-close-white" data-bs-dismiss="offcanvas" aria-label="Close"></button>
              </div>
		      <div class="offcanvas-body d-lg-none">
                <ul class="navbar-nav justify-content-end flex-grow-1 pe-3">
          <li class="nav-item">
            <a class="nav-link" href="<?php echo e(url('')); ?>"><i class="fa-solid fa-house"></i> Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo e(url('/cari')); ?>"><i class="fa-solid fa-magnifying-glass"></i> Cek Pesanan</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo e(url('/login')); ?>"><i class="fa-solid fa-arrow-right-to-bracket"></i> Login</a>
          </li>
                    </ul>
    </div>
  </div>
    <div class="collapse navbar-collapse text-right d-none d-md-none d-lg-block">
      <div class="navbar-nav ms-auto nav-stacked">
        <a class="nav-link" href="<?php echo e(url('')); ?>"><i class="fa-solid fa-house"></i> Home</a>
        <a class="nav-link" href="<?php echo e(url('/cari')); ?>"><i class="fa-solid fa-magnifying-glass""></i> Cek Pesanan</a>
         <a class="nav-link" href="<?php echo e(url('/login')); ?>"><i class="fa-solid fa-arrow-right-to-bracket""></i> Login</a>
</div>
  </div>
</nav>
<?php endif; ?>


<div style="margin: 0 auto;max-width: 1140px; padding-top: 80px;">
  <div class="row">
      <div class="col-lg-4">
        <div class="card bg-card border-0 mb-4 shadow-form">
        <div class="games-banner rounded-top"></div>
        <div class="col-12 px-3 pb-2">
          <img src="<?php echo e($kategori->thumbnail); ?>" alt="" width="100" class="float-start mr-2" style="border-radius: 16px; margin-top: -50px;">
          <div class="col-8 mt-2 mt-lg-0 float-end">
            <h4><?php echo e($kategori->nama); ?></h4>
          </div>
        </div>
        <div class="col px-3 mb-4">
            <small style="font-size: 14px;"><?php echo $kategori->deskripsi_game; ?></small>
        </div>
      </div>
    </div>
    <div class="col-lg-8">
      
      <form id="formSubmitPengajuan">
        
        <div class="card bg-card border-0 shadow-form">			
          <div class="px-3 pt-3" style="margin-bottom: 15px;">
              <div class="num-page border-bottom">
                <div>1</div>
                <p>Informasi Data Diri</p>
              </div>
              <div class="row">
                  <div class="col-6">
                    <label for="">Nama Lengkap</label>
                    <input type="text" class="form-control" placeholder="Masukkan Nama Lengkap Anda" id="fullname" name="fullname" autocomplete="off">
                  </div>
                  <div class="col-6">
                  <label for="">Email</label>
                    <input type="text" class="form-control" placeholder="Masukkan Email Anda" id="email" name="email" autocomplete="off">
                  </div>
                  <div class="col-6">
                    <label for="">No Telephone</label>
                    <input type="text" class="form-control" placeholder="Masukkan No Telephone Anda" id="phone_number" name="phone_number" autocomplete="off">
                  </div>
                  <div class="col-6">
                    <label for="">Jenis Kelamin</label>
                    <select name="jenis_kelamin" id="jenis_kelamin" class="form-control">
                      <option value="laki-laki">Laki Laki</option>
                      <option value="perempuan">Perempuan</option>
                    </select>
                  </div>
                  <div class="col-6">
                    <label for="">Tempat Tanggal Lahir</label>
                    <input type="text" class="form-control" placeholder="Tempat Tanggal Lahir" id="place_birth" name="place_birth" autocomplete="off">
                  </div>
                  <div class="col-6">
                    <label for="">Tanggal Lahir</label>
                    <input type="date" class="form-control" id="date_birth" name="date_birth" autocomplete="off">
                  </div>
                  <div class="col-6">
                    <label for="">Provinsi</label>
                    <select name="jenis_kelamin" id="provinsi" class="form-control">
                      <option value=""></option>
                    </select>
                  </div>
                  <div class="col-6">
                    <label for="">Kabupaten / Kota</label>
                    <select name="jenis_kelamin" id="kabupaten" class="form-control">
                      <option value=""></option>
                    </select>
                  </div>
                  <div class="col-6">
                    <label for="">Kecamatan</label>
                    <select name="jenis_kelamin" id="kecamatan" class="form-control">
                      <option value=""></option>
                    </select>
                  </div>
                  <div class="col-6">
                    <label for="">Kelurahan</label>
                    <select name="jenis_kelamin" id="kelurahan" class="form-control">
                      <option value=""></option>
                    </select>
                  </div>
                  <div class="col-12">
                    <label for="">Alamat Sesuai KTP</label>
                    <textarea name="alamat_ktp" id="alamat_ktp" class="form-control" cols="30" rows="5"></textarea>
                  </div>
                  <div class="col-12">
                    <label for="">Alamat Tempat Tinggal Sekarang</label>
                    <textarea name="alamat_now" id="alamat_now" class="form-control" cols="30" rows="5"></textarea>
                  </div>
                </div>
          </div>
        </div>
        <br>
        
        <div class="card bg-card border-0 shadow-form">
          <div class="px-3 pt-3" style="margin-bottom: 15px;">
            <div class="num-page border-bottom">
              <div>1</div>
              <p>Pekerjaan</p>
            </div>
            <div class="row">
              <div class="col-6">
                <label for="">Nama Perusahaan</label>
                <input type="text" class="form-control" placeholder="Masukkan Nama Perusahaan Anda" id="company_name" name="company_name" autocomplete="off">
              </div>
              <div class="col-6">
                <label for="">Alamat Perusahaan</label>
                <input type="text" class="form-control" placeholder="Masukkan Alamat Perusahaan Anda" id="company_address" name="company_address" autocomplete="off">
              </div>
              <div class="col-6">
                <label for="">No Telepon Kantor</label>
                <input type="text" class="form-control" placeholder="Masukkan No Telepon Anda" id="company_phone" name="company_phone" autocomplete="off">
              </div>
              <div class="col-6">
                <label for="">Penghasilan</label>
                <input type="text" class="form-control" placeholder="Masukkan Pendapatan Anda" id="company_income" name="company_income" autocomplete="off">
              </div>
            </div>
          </div>
        </div>
        <br>
        
        <div class="card bg-card border-0 shadow-form">
          <div class="px-3 pt-3" style="margin-bottom: 15px;">
            <div class="num-page border-bottom">
              <div>1</div>
              <p>Kontak Darurat</p>
            </div>
            <div class="row">
              <div class="col-6">
                <label for="">Nama Econ 1</label>
                <input type="text" class="form-control" placeholder="Masukkan Nama Econ" id="name_econ1" name="name_econ1" autocomplete="off">
              </div>
              <div class="col-6">
                <label for="">No Phone Econ 1</label>
                <input type="text" class="form-control" placeholder="Masukkan No Telepon" id="phone_econ1" name="phone_econ1" autocomplete="off">
              </div>
              <div class="col-6">
                <label for="">Name Econ 2</label>
                <input type="text" class="form-control" placeholder="Masukkan Nama Econ" id="name_econ2" name="name_econ2" autocomplete="off">
              </div>
              <div class="col-6">
                <label for="">No Phone Econ 2</label>
                <input type="text" class="form-control" placeholder="Masukkan No Telepon" id="phone_econ2" name="phone_econ2" autocomplete="off">
              </div>
            </div>
          </div>
        </div>
        <br>
        
        <div class="card bg-card border-0 shadow-form">
          <div class="px-3 pt-3" style="margin-bottom: 15px;">
            <div class="num-page border-bottom">
              <div>1</div>
              <p>Dokumen</p>
            </div>
            <input type="hidden" name="lat_long" id="lat_long" class="setLatLongKordinat">
            <input type="hidden" name="imageSelfieKTP" id="imageSelfieKTP" class="setLatLongKordinat">
            <div class="row">
              <label for="">Selfie KTP</label>
              <div class="col-12">
                  <i class="bi bi-camera font-14"></i>
                  <select class="form-control" name="select_camera" id="select_camera">
                      <option value="">-- Silahkan Pilih Camera --</option>
                      <option value="depan">Camera Depan</option>
                      <option value="belakang">Camera Belakang</option>
                  </select>
              </div>
              <div class="col-12" id="resultCam">
                  <video id="cam" autoplay muted playsinline>Not available</video>
                  <canvas id="canvas" style="display:none"></canvas>
                  <img width="1" height="1" id="photo" alt="The screen capture will appear in this box.">
              </div>
            </div>
            <div class="row" style="margin-right: 30px; margin-left: 30px; margin-bottom: 50px;">
                <button type="button" id="snapBtnDokumenSelfie" class="btn btn-block btn-xl btn-secondary">
                    <span style="font-size: 11px;">Take Snapshot</span>
                </button>
            </div>

            <div class="row">
              <div class="col-12">
                <label for="">Foto E-KTP</label>
                <input type="file" class="form-control" placeholder="Foto KTP" id="foto_ktp" name="foto_ktp" autocomplete="off">
              </div>
              <div class="col-12">
                <label for="">Dokumen Pendukung NPWP/SIM/BPJS/KIS (Pilih salah satu)</label>
                <input type="file" class="form-control" placeholder="Foto KTP" id="foto_dokumen_pendukung" name="foto_dokumen_pendukung" autocomplete="off">
              </div>
              <!-- <div class="col-12">
                <label for="">Dokumen Pendukung ID Card/Kartu Nama/Slip Gaji (Pilih salah satu)</label>
                <input type="file" class="form-control" placeholder="Foto KTP" id="foto_ktp" name="foto_ktp" autocomplete="off">
              </div> -->
            </div>
          </div>
        </div>
        <br>
        <div class="row" style="margin-right: 40px; margin-left: 40px; margin-bottom: 50px;">
            <button type="button" id="btnSubmitPengajuan" class="btn btn-block btn-xl btn-primary">
                <span style="font-size: 20px; font-weight: bold;">Submit Pengajuan !</span>
            </button>
        </div>
      </form>

    </div>
  </div>
</div>

<?php $__env->startPush('custom_script'); ?>

<script>

$(document).ready(function(){
    function provice() {
      $.ajax({
        url: "<?php echo e(url('provinsi')); ?>", // Replace with your server-side script to fetch provinces
        method: 'GET',
        dataType: 'json',
        success: function(data) {
          var provinceSelect = $('#provinsi');
          provinceSelect.empty();

          $.each(data.provinsi, function(index, province) {
            provinceSelect.append('<option value="' + province.id + '">' + province.nama + '</option>');
          });
        }
      });
    }
    provice();

    $('#provinsi').change(function() {
      var selectedProvince = $(this).val();
      var regencySelect = $('#kabupaten');
      regencySelect.empty();

      if (selectedProvince !== '') {
        $.ajax({
          url: "<?php echo e(url('kota')); ?>" + "/" + selectedProvince, // Replace with your server-side script to fetch regencies based on province ID
          method: 'GET',
          dataType: 'json',
          success: function(data) {
            console.log(data)
            $.each(data.kota_kabupaten, function(index, regency) {
              regencySelect.append('<option value="' + regency.id + '">' + regency.nama + '</option>');
            });
          }
        });
      }
    });

    $('#kabupaten').change(function() {
      var selectedRegency = $(this).val();
      var citySelect = $('#kecamatan');
      citySelect.empty();

      if (selectedRegency !== '') {
        $.ajax({
          url: "<?php echo e(url('kecamatan')); ?>" + "/" + selectedRegency, // Replace with your server-side script to fetch regencies based on province ID
          method: 'GET',
          dataType: 'json',
          success: function(data) {
            $.each(data.kecamatan, function(index, city) {
              citySelect.append('<option value="' + city.id + '">' + city.nama + '</option>');
            });
          }
        });
      }
    });

    $('#kecamatan').change(function() {
      var selectedCity = $(this).val();
      var citySelect = $('#kelurahan');
      citySelect.empty();

      if (selectedCity !== '') {
        $.ajax({
          url: "<?php echo e(url('kelurahan')); ?>" + "/" + selectedCity, // Replace with your server-side script to fetch regencies based on province ID
          method: 'GET',
          dataType: 'json',
          success: function(data) {
            $.each(data.kelurahan, function(index, city) {
              citySelect.append('<option value="' + city.id + '">' + city.nama + '</option>');
            });
          }
        });
      }
    });

});

function redirectCountDown(idStatusCounter = 'secondsCounterSuccess'){
    var el = document.getElementById(idStatusCounter),
    total = el.innerHTML,
    timeinterval = setInterval(function () {
        total = --total;
        el.textContent = total;
        if (total <= 0) {
            window.location.reload()
        }
    }, 1000);
}

// Locations ####
// getLocationInterval()
var options = {
    enableHighAccuracy: true,
    maximumAge: 10000,
    timeout: 5000
};
function showError(error) {
    switch (error.code) {
        case error.PERMISSION_DENIED:
        // $('#snapBtnDokumenSelfie').prop("disabled", true)
        console.log("Silahkan Aktifkan Gps Anda,Jika Sudah Silahkan Refresh");
        // alert("Silahkan Aktifkan Gps Anda,Jika Sudah Silahkan Refresh")
        // redirectCountDown('secondsCounterFailed')

        toastr.warning('Silahkan Aktifkan Gps/Lokasi Anda,Jika Sudah Silahkan Refresh');

        // x.innerHTML = "Silahkan Aktifkan Gps Anda,Jika Sudah Silahkan Refresh"
        break;
        case error.POSITION_UNAVAILABLE:
        x.innerHTML = "Location information is unavailable."
        break;
        case error.TIMEOUT:
        x.innerHTML = "The request to get user location timed out."
        break;
        case error.UNKNOWN_ERROR:
        x.innerHTML = "An unknown error occurred."
        break;
    }
}
/* Get Location Tanpa Interval */
function getLocationNoneInterval() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition, showError, options);
    } else {
        x.innerHTML = "Geolocation is not supported by this browser.";
    }
}

/* Get Location Interval */
function getLocationInterval() {
    setInterval(() => {
      if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition, showError, options);
      } else {
        console.log("Geolocation is not supported by this browser");
      }
    }, 2000);
  }

/* Show Postion */
function showPosition(position) {
    console.log(position.coords.latitude + ',' + position.coords.longitude)
    $('.setLatLongKordinat').val(position.coords.latitude + ',' + position.coords.longitude)
}
// End Location ####
</script>

<script>
    // reference to the current media stream
    var mediaStream = null;
    // $("#photo").remove()
    // Prefer camera resolution nearest to 1280x720.
    var constraints = {
        audio: false,
        video: {
            width: {
                ideal: 448
            },
            height: {
                ideal: 300
            },
            flip_horiz: true,
            align: 'center',
            facingMode: "environment",
            fps: 60
        }
    };

    async function getMediaStream(constraints) {
        try {
            mediaStream = await navigator.mediaDevices.getUserMedia(constraints);
            let video = document.getElementById('cam');
            video.srcObject = mediaStream;
            video.onloadedmetadata = (event) => {
                video.play();
            };
        } catch (err) {
            console.error(err.message);
        }
    };

    async function switchCamera(cameraMode) {
        try {
            // stop the current video stream
            if (mediaStream != null && mediaStream.active) {
                var tracks = mediaStream.getVideoTracks();
                tracks.forEach(track => {
                    track.stop();
                })
            }

            // set the video source to null
            document.getElementById('cam').srcObject = null;

            // change "facingMode"
            constraints.video.facingMode = cameraMode;

            // get new media stream
            await getMediaStream(constraints);
        } catch (err) {
            console.error(err.message);
            alert(err.message);
        }
    }

    function takePicture() {
        let canvas = document.getElementById('canvas');
        let video = document.getElementById('cam');
        let photo = document.getElementById('photo');
        let context = canvas.getContext('2d');

        const height = video.videoHeight;
        const width = video.videoWidth;

        if (width && height) {
            $("#canvas").remove()
            $("#cam").remove()
            $("#photo").attr('width', 448).attr('height', 300);
            canvas.width = width;
            canvas.height = height;
            context.drawImage(video, 0, 0, width, height);
            var data = canvas.toDataURL('image/png');
            photo.setAttribute('src', data);

            // Submit Absen
            $("#imageSelfieKTP").val(data)
            // ajaxSubmitAbsen(data);

        } else {
            clearphoto();
        }
    }

    function clearPhoto() {
        let canvas = document.getElementById('canvas');
        let photo = document.getElementById('photo');
        let context = canvas.getContext('2d');

        context.fillStyle = "#AAA";
        context.fillRect(0, 0, canvas.width, canvas.height);
        var data = canvas.toDataURL('image/png');
        photo.setAttribute('src', data);
    }

    $(document).ready(function() {
        $('#snapBtnDokumenSelfie').prop('disabled', true);

        $('#select_camera').on('change', function() {
            let value = $(this).val();
            if (value == 'depan') {
                switchCamera('user');
                $('#snapBtnDokumenSelfie').prop('disabled', false);
            } else {
                switchCamera('environment');
                $('#snapBtnDokumenSelfie').prop('disabled', false);
            }
        });
    });

    // document.getElementById('switchFrontBtn').onclick = (event) => {
    //     switchCamera("user");
    // }

    // document.getElementById('switchBackBtn').onclick = (event) => {
    //     switchCamera("environment");
    // }

    document.getElementById('snapBtnDokumenSelfie').onclick = (event) => {
        takePicture();
        event.preventDefault();
    }

    clearPhoto();
</script>


<script>

$("#btnSubmitPengajuan").click(function() {
      
      let foto_ktp               = $('#foto_ktp').prop('files')[0];
      let foto_dokumen_pendukung = $('#foto_dokumen_pendukung').prop('files')[0];
      var formData = new FormData($("#formSubmitPengajuan")[0]);
      formData.append("foto_ktp", foto_ktp);
      formData.append("foto_dokumen_pendukung", foto_dokumen_pendukung);
      formData.append("submit-pengajuan", "submit-pengajuan");
      formData.append("_token", "<?php echo e(csrf_token()); ?>");

      $.ajax({
        url: "<?php echo e(route('pangajuan.pinjaman')); ?>",
        type: "POST",
        data: formData,
        processData: false,
        contentType: false,
        success: function(data) {
          console.log(data);
        },
        error: function(data) {
          console.log(data);
        },
        complete: function(data) {
          console.log(data);
        }
      })

    })

</script>

<?php $__env->stopPush(); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('template.template', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /private/var/www/DigiTopup/mcproject/resources/views/template/allconvertpinjaman.blade.php ENDPATH**/ ?>